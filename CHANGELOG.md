# Focal Point Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 1.0.3 - 2021-08-31
### Fixed
- Fixes an annoying JavaScript issue that would put the initial focal point BOTTOM LEFT!!!1

## 1.0.0 - 2018-11-14
### Added
- Initial release
