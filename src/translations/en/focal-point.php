<?php
/**
 * Focal Point plugin for Craft CMS 3.x
 *
 * Lorem
 *
 * @link      www.vaersaagod.no
 * @copyright Copyright (c) 2018 Værsågod
 */

/**
 * @author    Værsågod
 * @package   FocalPoint
 * @since     1.0.0
 */
return [
    'Focal Point plugin loaded' => 'Focal Point plugin loaded',
];
