<?php
/**
 * Focal Point plugin for Craft CMS 3.x
 *
 * Lorem
 *
 * @link      www.vaersaagod.no
 * @copyright Copyright (c) 2018 Værsågod
 */

namespace vaersaagod\focalpoint\fields;

use vaersaagod\focalpoint\FocalPoint;
use vaersaagod\focalpoint\assetbundles\focalpointfieldfield\FocalPointFieldFieldAsset;

use Craft;
use craft\base\ElementInterface;
use craft\base\Field;
use craft\helpers\Db;
use yii\db\Schema;
use craft\helpers\Json;
use craft\elements\Asset;

/**
 * @author    Værsågod
 * @package   FocalPoint
 * @since     1.0.0
 */
class FocalPointField extends Field
{
    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $defaultFocalPoint = '50% 50%';

    /**
     * @var array|null
     */
    public $defaultPointArray;

    // Static Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('focal-point', 'FocalPoint Field');
    }

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules = array_merge($rules, [
            ['defaultFocalPoint', 'string'],
            ['defaultFocalPoint', 'default', 'value' => json_encode(['x' => '50', 'y' => '50', 'css' => '50% 50%'], true)],
        ]);
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function getContentColumnType(): string
    {
        return Schema::TYPE_STRING;
    }

    /**
     * @inheritdoc
     */
    public function normalizeValue($value, ElementInterface $element = null)
    {
        if (\is_string($value)) {
            $value = json_decode($value, true);
        }
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function serializeValue($value, ElementInterface $element = null)
    {
        return parent::serializeValue($value, $element);
    }

    /**
     * @inheritdoc
     */
    public function getSettingsHtml()
    {
        // Render the settings template
        return Craft::$app->getView()->renderTemplate(
            'focal-point/_components/fields/FocalPointField_settings',
            [
                'field' => $this,
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getInputHtml($value, ElementInterface $element = null): string
    {
        // Register our asset bundle
        Craft::$app->getView()->registerAssetBundle(FocalPointFieldFieldAsset::class);

        // Get our id and namespace
        $id = Craft::$app->getView()->formatInputId($this->handle);
        $namespacedId = Craft::$app->getView()->namespaceInputId($id);

        // Variables to pass down to our field JavaScript to let it namespace properly
        $jsonVars = [
            'id' => $id,
            'name' => $this->handle,
            'namespace' => $namespacedId,
            'prefix' => Craft::$app->getView()->namespaceInputId(''),
        ];
        $jsonVars = Json::encode($jsonVars);
        Craft::$app->getView()->registerJs("$('#{$namespacedId}-field').FocalPointFocalPointField(" . $jsonVars . ");");

        // Render the input template
        return Craft::$app->getView()->renderTemplate(
            'focal-point/_components/fields/FocalPointField_input',
            [
                'name' => $this->handle,
                'value' => $value,
                'field' => $this,
                'id' => $id,
                'namespacedId' => $namespacedId,
                'asset' => $element->owner instanceof Asset ? $element->owner : null,
            ]
        );
    }
}
